package nl.utwente.di.bookQuote;

public class Quoter {
    public double getFahrenheit(String celcius){
        double degree = Double.parseDouble(celcius);
        return (degree*1.8)+32;
    }
}
